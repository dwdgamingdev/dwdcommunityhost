package net.downwithdestruction.dwdcommunityhost;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.util.Date;
import net.downwithdestruction.dwdcommunityhost.server.Server;

public class DwDCommunityHost {

    private static int port = 4209;
    private static String ip = "127.0.0.1";
    private static String version = "0.0.1-indev";
    private static Server server;
    //private static Client client;
    private static DwDCommunityHost host;

    public static void main(String[] args) throws IOException {
        host = new DwDCommunityHost();
    }

    public DwDCommunityHost() {
        try {
            ServerSocket sSocket = new ServerSocket(4444);
            System.out.println("Starting DwDCommunityHost Server v" + version);
            System.out.println("Server started at: " + new Date());

            System.out.println("Initialising Server...");
            startServer();
            System.out.println("Server initialised.");

            System.out.println("Initialising Client...");
            System.out.println("Client initialised.");

            KeepAlive.in = new BufferedReader(new InputStreamReader(System.in));

            // creating a new thread to handle the input  
            Thread t1 = new Thread(new KeepAlive());
            t1.setDaemon(false);
            t1.start();

            while (true) {
                try {
                    t1.sleep(10);       // main-thread is sleeping not the thread created above  
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // outputting data until the quit boolean flag is not set  
                if (KeepAlive.quit == true) {
                    break;
                }

            }

        } catch (IOException exception) {
            System.out.println("Error: " + exception);
        }
    }

    private static void startServer() {
        server = new Server();
        server.start();
    }

    public static int getPort() {
        return port;
    }
}
