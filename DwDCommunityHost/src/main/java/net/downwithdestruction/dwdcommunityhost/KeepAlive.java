/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdcommunityhost;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Dan
 */
public class KeepAlive implements Runnable {  
      
    // declaring reader to read the keyboard and flag to notify main thread to stop when set  
    public static BufferedReader in ;   
    public static boolean quit=false;  
      
    public void run(){  
        String msg = null;  
          
        // threading is waiting for the key Q to be pressed   
        while(true){  
            try{  
            msg=in.readLine();  
            }catch(IOException e){  
                e.printStackTrace();  
                }  
              
            if(msg.equals("Q")) {quit=true;break;}  
        }  
    }  
}  
