/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdcommunityhost.packet;

import java.io.Serializable;
import net.downwithdestruction.dwdcommunityhost.server.Connection;

/**
 *
 * @author Dan
 */
public class Packet implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private int type;
    private Connection connection;
    private Object args = null;
    
    public Packet(PacketType type, Object args) {
        this.type = type.getType();
        this.args = args;
    }
    
    public Packet(PacketType type) {
        this.type = type.getType();
    }
    
    public Packet(int type, Object args) {
        this.type = type;
        this.args = args;
    }
    
    public Packet(int type) {
        this.type = type;
    }
    
    
    
}
