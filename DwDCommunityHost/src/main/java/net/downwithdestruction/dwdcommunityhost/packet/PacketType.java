/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdcommunityhost.packet;

/**
 *
 * @author Dan
 */
public enum PacketType {
    SERVER_DISCONNECT(1),
    SERVER_NAME(2),
    AUTHENTICATION(3),
    PING(4),
    PONG(5),
    CLIENT_DISCONNECT(6),
    CLIENT_CONNECT(7),
    CLOSE_CONNECTION(8),
    PLAYER_JOIN(9),
    PLAYER_LEAVE(10),
    PLAYER_KICK(11),
    PLAYER_BAN(12);
    
    private int type;
    
    PacketType(int type) {
        this.type = type;
    }
    
    public int getType() {
        return type;
    }
}
