/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdcommunityhost.server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import net.downwithdestruction.dwdcommunityhost.packet.Packet;
import net.downwithdestruction.dwdcommunityhost.packet.PacketType;

/**
 *
 * @author Dan
 */
public class Connection extends Thread {
    
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    
    private Socket clientSocket;
    
    private boolean connectionStatus = false;
    private String name = "";
    
    public Connection(Socket clientSocket) {
        this.clientSocket = clientSocket;
        System.out.println("Adding Client");
        connectionStatus = true;
    }
    
    @Override
    public void run() {
        while(connectionStatus == true) {
            try {
                setInput(new ObjectInputStream(getSocket().getInputStream()));
                parseResponse((Packet) getInput().readObject());
            } catch (Exception e) {}
        }
    }
    
    public void send(Packet p) {
        try {
            setOutput(new ObjectOutputStream(getSocket().getOutputStream()));
            getOutput().writeObject(p);
        } catch (Exception e) {}
    }
    
    public void parseResponse(Packet p) {
        
    }
    
    public void closeConnection() {
        connectionStatus = false;
        
        try {
            send(new Packet(PacketType.CLOSE_CONNECTION));
            getOutput().close();
            getInput().close();
            getSocket().close();
        } catch (Exception e) {}
    }
    
    
    
    public ObjectInputStream getInput() {
        return inputStream;
    }
    
    public ObjectOutputStream getOutput() {
        return outputStream;
    }
    
    public void setInput(ObjectInputStream stream) {
        this.inputStream = stream;
    }
    
    public void setOutput(ObjectOutputStream stream) {
        this.outputStream = stream;
    }
    
    public Socket getSocket() {
        return clientSocket;
    }
    
}
