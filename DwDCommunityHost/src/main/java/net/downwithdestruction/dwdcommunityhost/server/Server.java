/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.downwithdestruction.dwdcommunityhost.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import net.downwithdestruction.dwdcommunityhost.DwDCommunityHost;
import net.downwithdestruction.dwdcommunityhost.packet.Packet;
import net.downwithdestruction.dwdcommunityhost.packet.PacketType;

/**
 *
 * @author Dan
 */
public class Server extends Thread {
    
    private static ArrayList<Connection> clients = new ArrayList<Connection>();
    
    private ServerSocket serverSocket;
    private Socket clientSocket;
    
    private boolean connectionStatus = false;
    
    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(DwDCommunityHost.getPort());
        } catch(Exception e) {}
        
        while(connectionStatus == true) {
            try {
                clientSocket = serverSocket.accept();
                System.out.println("Connection Established on :" + DwDCommunityHost.getPort());
                
                Connection connection = new Connection(clientSocket);
                clients.add(connection);
                connection.start();
            } catch(Exception e) {
                System.out.println("Exception in server thread");
            }
        }
    }

    public static void sendPacket(Packet p, Connection connection) {
        for(Connection conn : clients) {
            if(conn != connection) {
                conn.send(p);
            }
        }
    }
    
    public void closeConnections() {
        connectionStatus = false;
        try {
            for(Connection conn : clients) {
                conn.send(new Packet(PacketType.SERVER_DISCONNECT));
                conn.closeConnection();
            }
            
            for(int x=0; x<clients.size();x++) {
                clients.remove(x);
            }
            
            serverSocket.close();
            clientSocket.close();
        } catch (Exception e) {}
    }
    
    public static void closeConnection(Connection connection) {
        connection.closeConnection();
    }
    
}
