package net.downwithdestruction.xserverchat.Server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import net.downwithdestruction.xserverchat.packets.Packet;
import net.downwithdestruction.xserverchat.packets.PacketTypes;

public class Connection extends Thread {

    private ObjectInputStream in;
    private ObjectOutputStream out;
    private Socket skt;
    private boolean open = true;
    private String name = "";
    private int sent = 0;
    private int recived = 0;

    public Connection(Socket skt2) {

        this.skt = skt2;
        System.out.println("Adding Client");
        open = true;
    }

    public void run() {
        while (open) {
            try {
                setIn(new ObjectInputStream(getSkt().getInputStream()));
                parse((Packet) getIn().readObject());
                sent++;
            } catch (Exception e) {}
        }

    }

    public void send(Packet p) {
        try {
            setOut(new ObjectOutputStream(getSkt().getOutputStream()));
            getOut().writeObject(p);
            recived++;
        } catch (Exception e) {
        }
    }

    public void parse(Packet p) {

        if(p.getType() == PacketTypes.PACKET_CLIENT_CONNECTED){
            name = (String)p.getArgs();
            Server.sendPacket(p, this);
        }
        else if (p.getType() == PacketTypes.PACKET_STATS_REQ){
            System.out.println("REQ_STATS");
            Server.genAndSendStats(this);
        }
        else if(p.getType() == PacketTypes.PACKET_CLIENT_DC){
            Server.sendPacket(p, this);
            Server.closeConnection(this);
        }
        else {
            Server.sendPacket(p, this);
        }

    }


    public void closeConnection(){
        open = false;

        try{
            send(new Packet(PacketTypes.PACKET_CC, null));
            getOut().close();
            getIn().close();
            getSkt().close();
        }catch(Exception e){}
    }

    public boolean isOpen(){
        return open;
    }
    public String getClientName(){
        return name;
    }
    public int getSent(){
        return sent;
    }
    public int getRecived(){
        return recived;
    }

    /**
     * @return the in
     */
    public ObjectInputStream getIn() {
        return in;
    }

    /**
     * @param in the in to set
     */
    public void setIn(ObjectInputStream in) {
        this.in = in;
    }

    /**
     * @return the out
     */
    public ObjectOutputStream getOut() {
        return out;
    }

    /**
     * @param out the out to set
     */
    public void setOut(ObjectOutputStream out) {
        this.out = out;
    }

    /**
     * @return the skt
     */
    public Socket getSkt() {
        return skt;
    }

    /**
     * @param skt the skt to set
     */
    public void setSkt(Socket skt) {
        this.skt = skt;
    }
}
