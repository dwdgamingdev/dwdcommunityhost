package net.downwithdestruction.xserverchat;

import java.util.HashMap;
import net.downwithdestruction.xserverchat.Server.Server;
import net.downwithdestruction.xserverchat.client.ChatListener;
import net.downwithdestruction.xserverchat.client.Client;
import net.downwithdestruction.xserverchat.packets.Packet;
import net.downwithdestruction.xserverchat.packets.PacketTypes;
import net.downwithdestruction.xserverchat.util.LogManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * @
 * 
 * 
 * Authors:
 * @author Drew  [ https://github.com/Double0negative ]
 * 
 */
public class XServer extends JavaPlugin {

    private static String version = "0.2.6";
    private static ChatColor color = ChatColor.WHITE;
    private static ChatColor seccolor = ChatColor.WHITE;
    private static ChatColor aColor = ChatColor.AQUA;
    private static ChatColor pColor = ChatColor.GOLD;
    private static ChatColor eColor = ChatColor.DARK_RED;
    private static String pre = "[XServer] ";
    private static String xpre = pColor+pre;
    private static String ip;
    private static  int port;
    private static String prefix;
    private static String serverName;
    private static boolean isHost = false;

    /**
     * @return the version
     */
    public static String getVersion() {
        return version;
    }

    /**
     * @param aVersion the version to set
     */
    public static void setVersion(String aVersion) {
        version = aVersion;
    }

    /**
     * @return the color
     */
    public static ChatColor getColor() {
        return color;
    }

    /**
     * @param aColor the color to set
     */
    public static void setColor(ChatColor aColor) {
        color = aColor;
    }

    /**
     * @return the seccolor
     */
    public static ChatColor getSeccolor() {
        return seccolor;
    }

    /**
     * @param aSeccolor the seccolor to set
     */
    public static void setSeccolor(ChatColor aSeccolor) {
        seccolor = aSeccolor;
    }

    /**
     * @return the aColor
     */
    public static ChatColor getaColor() {
        return aColor;
    }

    /**
     * @param aaColor the aColor to set
     */
    public static void setaColor(ChatColor aaColor) {
        aColor = aaColor;
    }

    /**
     * @return the pColor
     */
    public static ChatColor getpColor() {
        return pColor;
    }

    /**
     * @param apColor the pColor to set
     */
    public static void setpColor(ChatColor apColor) {
        pColor = apColor;
    }

    /**
     * @return the eColor
     */
    public static ChatColor geteColor() {
        return eColor;
    }

    /**
     * @param aeColor the eColor to set
     */
    public static void seteColor(ChatColor aeColor) {
        eColor = aeColor;
    }

    /**
     * @return the pre
     */
    public static String getPre() {
        return pre;
    }

    /**
     * @param aPre the pre to set
     */
    public static void setPre(String aPre) {
        pre = aPre;
    }

    /**
     * @return the xpre
     */
    public static String getXpre() {
        return xpre;
    }

    /**
     * @param aXpre the xpre to set
     */
    public static void setXpre(String aXpre) {
        xpre = aXpre;
    }

    /**
     * @return the ip
     */
    public static String getIp() {
        return ip;
    }

    /**
     * @param aIp the ip to set
     */
    public static void setIp(String aIp) {
        ip = aIp;
    }

    /**
     * @return the port
     */
    public static int getPort() {
        return port;
    }

    /**
     * @param aPort the port to set
     */
    public static void setPort(int aPort) {
        port = aPort;
    }

    /**
     * @return the prefix
     */
    public static String getPrefix() {
        return prefix;
    }

    /**
     * @param aPrefix the prefix to set
     */
    public static void setPrefix(String aPrefix) {
        prefix = aPrefix;
    }

    /**
     * @return the serverName
     */
    public static String getServerName() {
        return serverName;
    }

    /**
     * @param aServerName the serverName to set
     */
    public static void setServerName(String aServerName) {
        serverName = aServerName;
    }

    /**
     * @return the isHost
     */
    public static boolean isIsHost() {
        return isHost;
    }

    /**
     * @param aIsHost the isHost to set
     */
    public static void setIsHost(boolean aIsHost) {
        isHost = aIsHost;
    }

    /**
     * @return the netActive
     */
    public static boolean isNetActive() {
        return netActive;
    }

    /**
     * @param aNetActive the netActive to set
     */
    public static void setNetActive(boolean aNetActive) {
        netActive = aNetActive;
    }

    /**
     * @return the restartMode
     */
    public static int getRestartMode() {
        return restartMode;
    }

    /**
     * @param aRestartMode the restartMode to set
     */
    public static void setRestartMode(int aRestartMode) {
        restartMode = aRestartMode;
    }

    /**
     * @return the dc
     */
    public static boolean isDc() {
        return dc;
    }

    /**
     * @param aDc the dc to set
     */
    public static void setDc(boolean aDc) {
        dc = aDc;
    }

    /**
     * @return the hostdc
     */
    public static boolean isHostdc() {
        return hostdc;
    }

    /**
     * @param aHostdc the hostdc to set
     */
    public static void setHostdc(boolean aHostdc) {
        hostdc = aHostdc;
    }

    /**
     * @return the formats
     */
    public static HashMap<String, String> getFormats() {
        return formats;
    }

    /**
     * @param aFormats the formats to set
     */
    public static void setFormats(HashMap<String, String> aFormats) {
        formats = aFormats;
    }

    /**
     * @return the override
     */
    public static HashMap<String, String> getOverride() {
        return override;
    }

    /**
     * @param aOverride the override to set
     */
    public static void setOverride(HashMap<String, String> aOverride) {
        override = aOverride;
    }
    private Server server;
    private Client client;
    private static boolean netActive = true;
    private static int restartMode = 0;
    private static boolean dc = false;
    private static boolean hostdc = false;
    private static Player stat_req = null;
    private ChatListener cl = new ChatListener();
    private static HashMap<String, String>formats = new HashMap<String,String>();
    private static HashMap<String, String>override = new HashMap<String,String>();
    private static boolean formatoveride = false;

    @Override
    public void onEnable(){

        setNetActive(true);
        LogManager log = LogManager.getInstance();
        log.setup(this);
        log.info("XServer Version "+getVersion()+" Initializing");

        getConfig().options().copyDefaults(true);
        this.saveDefaultConfig();
        setIp(getConfig().getString("ip"));
        setPort(getConfig().getInt("port"));
        setPrefix(getConfig().getString("prefix"));
        setIsHost(getConfig().getBoolean("host"));
        setServerName(getConfig().getString("serverName"));

        getFormats().put("MESSAGE", getConfig().getString("formats.Message"));
        getFormats().put("LOGIN", getConfig().getString("formats.Login"));
        getFormats().put("LOGOUT", getConfig().getString("formats.Logout"));
        getFormats().put("DEATH", getConfig().getString("formats.Death"));
        getFormats().put("CONNECT", getConfig().getString("formats.Connect"));
        getFormats().put("DISCONNECT", getConfig().getString("formats.Disconnect"));


        formatoveride = getConfig().getBoolean("override.enabled");
        getOverride().put("MESSAGE", getConfig().getString("override.Message"));
        getOverride().put("LOGIN", getConfig().getString("override.Login"));
        getOverride().put("LOGOUT", getConfig().getString("override.Logout"));
        getOverride().put("DEATH", getConfig().getString("override.Death"));
        getOverride().put("CONNECT", getConfig().getString("override.Connect"));
        getOverride().put("DISCONNECT", getConfig().getString("override.Disconnect"));




        if(isIsHost()){
            LogManager.getInstance().info("THIS SERVER IS HOST");
            startServer();
        }

        startClient();

        this.getServer().getPluginManager().registerEvents(cl, this);
    }
    String s = "";

    @Override
    public void onDisable()
    {

        setHostdc(false);
        setNetActive(false);
        setDc(false);

        if(getRestartMode() == PacketTypes.DC_TYPE_RELOAD){
            s = "Reload";
        }
        else if(getRestartMode() == PacketTypes.DC_TYPE_STOP){
            s = " Shutting Down";
        }
        dc();
        if(isIsHost())
            dcServer();

    }

    public void startClient(){
        if(!isDc()){
            client = new Client(this, getIp(), getPort());
            client.openConnection();
            cl.setClient(client);
        }
    }

    public void dc(){
        if(!isDc()){
            client.send(new Packet(PacketTypes.PACKET_MESSAGE, getaColor() + getPrefix() +" Disconnecting. "+((!s.equals(""))?"Reason: "+s:"")));
            client.stopClient();
            this.getServer().broadcastMessage(getaColor()+getPre()+"Disconnecting from host");


        }




    }

    public void reloadClient(){
        setDc(false);
        dc();
        startClient();
    }

    public void startServer(){
        if(!isHostdc()){
            LogManager.getInstance().info("Starting as Host");
            server = new Server();
            server.start();
            setNetActive(true);
        }

    }

    public void dcServer(){
        if(!isHostdc()){
            Server.sendPacket(new Packet(PacketTypes.PACKET_MESSAGE, geteColor() +"[XServer] Host Disconnecting."), null);
            server.closeConnections();
        }
    }

    public void reloadServer(){
        setHostdc(false);
        setNetActive(false);
        dc();
        dcServer();
        startClient();
        startServer();
    }

    public boolean onCommand(CommandSender sender, Command cmd1, String commandLabel, String[] args){
        String cmd = cmd1.getName();
        Player player = null;
        if (sender instanceof Player) {
            player = (Player) sender;
        }
        
        if(cmd.equalsIgnoreCase("xserver") || cmd.equalsIgnoreCase("x")){ 
            if(args[0].equalsIgnoreCase("list")){
                stat_req = player;
                getStats();
            }
            if(player.isOp()){
                if(args[0].equalsIgnoreCase("dc") || args[0].equalsIgnoreCase("disconnect")){
                    if(isDc()){
                        player.sendMessage(getXpre()+"Already Disconnected!");
                    }
                    else{
                        dc();
                        setDc(true);

                        player.sendMessage(getXpre()+"Disconnected. You will be reconnected on next restart or with /x rc");
                    }
                }
                if(args[0].equalsIgnoreCase("rc") || args[0].equalsIgnoreCase("reload")){
                    reloadClient();
                    player.sendMessage(getXpre()+"Client Restarted");
                }
                if(args[0].equalsIgnoreCase("v") || args[0].equalsIgnoreCase("version")){
                    player.sendMessage(getXpre()+"Version: "+getVersion());
                }

                if(args[0].equalsIgnoreCase("host") || args[0].equalsIgnoreCase("server")){
                    if(args[1].equalsIgnoreCase("dc") || args[1].equalsIgnoreCase("disconnect")){
                        if(!isIsHost()){
                            player.sendMessage(getXpre()+"You are not host!");
                        }
                        else if(isHostdc()){
                            player.sendMessage(getXpre()+"Already Disconnected!");
                        }
                        else {
                            setHostdc(true);
                            dcServer();
                            player.sendMessage(getXpre()+"Server Shutdown! Restarting on next restart or with /x host rc");
                        }
                    }
                    if(args[1].equalsIgnoreCase("rc") || args[1].equalsIgnoreCase("reload") || args[1].equalsIgnoreCase("reconnect")){
                        if(!isIsHost()){
                            player.sendMessage(getXpre()+"You are not host!");
                        }
                        else {
                            reloadServer();
                            player.sendMessage(getXpre()+"Server Restarted!");
                        }

                    }
                }
            }

            return true;
        }
        return false;
    }

    public void getStats(){
        client.send(new Packet(PacketTypes.PACKET_STATS_REQ, null));
    }

    public static void msgStats(Object[][] stats){
        stat_req.sendMessage(getpColor()+"--------------XServer Chat Stats----------------");
        stat_req.sendMessage(getpColor()+"Server      Active      Packets Sent            Packets Recived");
        for(Object[] o:stats){
            String name = addspaces((String)o[0],25);
            String active = addspaces((Boolean) (o[1])?"true":"false",30);
            String sent = addspaces(o[2]+"",40);
            String rec = addspaces(o[3]+"",7);
            stat_req.sendMessage(getpColor()+name+active+sent+rec);
        }

    }

    public static String addspaces(String s, int sp){
        for(int a = 0;a< sp-s.length(); a++){
            s = s+" ";
        }
        return s;
    }

    public static String format(   HashMap<String, String>format, HashMap<String, String> val, String key){
        String str = "";
        if(!formatoveride){
            str = format.get(key);
        }else{
            str = getOverride().get(key);
        }

        str = str.replaceAll("\\{message\\}",(val.get("MESSAGE") != null)? val.get("MESSAGE"): "");
        str = str.replaceAll("\\{username\\}", (val.get("USERNAME") != null)? val.get("USERNAME"): "");
        str = str.replaceAll("\\{server\\}", (val.get("SERVERNAME") != null)? val.get("SERVERNAME"): "");

       str =str.replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
       
        return str;
    }

}
