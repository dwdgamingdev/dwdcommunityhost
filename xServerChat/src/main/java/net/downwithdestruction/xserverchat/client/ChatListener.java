package net.downwithdestruction.xserverchat.client;

import java.util.HashMap;
import net.downwithdestruction.xserverchat.XServer;
import net.downwithdestruction.xserverchat.packets.Packet;
import net.downwithdestruction.xserverchat.packets.PacketTypes;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ChatListener implements Listener {

    Client c;

    public void setClient(Client c) {
        this.c = c;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void handleChat(AsyncPlayerChatEvent event) {
        if(event.isCancelled())
            return;
        String msg =event.getMessage().replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
        event.setMessage(msg);
        c.sendMessage(event.getMessage(), event.getPlayer().getDisplayName());

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void handleCommand(PlayerCommandPreprocessEvent event) {

        if (event.getMessage().equalsIgnoreCase("/reload")) {
            XServer.setRestartMode(PacketTypes.DC_TYPE_RELOAD);
        }
        if (event.getMessage().startsWith("/stop")) {
            XServer.setRestartMode(PacketTypes.DC_TYPE_STOP);
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void handlePlayerJoin(PlayerJoinEvent event) {

        HashMap<String, String> f = new HashMap<String, String>();

        f.put("USERNAME", event.getPlayer().getDisplayName());
        f.put("SERVERNAME", XServer.getServerName());
        c.send(new Packet(PacketTypes.PACKET_PLAYER_JOIN, f));

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void handlePlayerLeave(PlayerQuitEvent event) {

        HashMap<String, String> f = new HashMap<String, String>();

        f.put("USERNAME", event.getPlayer().getDisplayName());
        f.put("SERVERNAME", XServer.getServerName());
        c.send(new Packet(PacketTypes.PACKET_PLAYER_LEAVE, f));
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void handlePlayerDeath(PlayerDeathEvent event) {

        HashMap<String, String> f = new HashMap<String, String>();

        f.put("USERNAME", event.getEntity().getDisplayName());
        f.put("SERVERNAME", XServer.getServerName());
        c.send(new Packet(PacketTypes.PACKET_PLAYER_DEATH, f));
    }

    /*
     * @EventHandler(priority = EventPriority.HIGH) public void
     * handleCommand(PlayerCommandPreprocessEvent event){
     * 
     * if(event.getMessage().equalsIgnoreCase("/reload")){ XServer.restartMode =
     * PacketTypes.DC_TYPE_RELOAD; } if(event.getMessage().startsWith("/stop")){
     * XServer.restartMode = PacketTypes.DC_TYPE_STOP; }
     * 
     * }
     */
}
